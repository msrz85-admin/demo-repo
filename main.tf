terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.8"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-west-2"
}

resource "aws_s3_bucket" "deviget-s3-bucket" {
  bucket = "deviget-test-bucket"

  lifecycle {
    prevent_destroy = true
  }
  
  tags = {
    Name        = "Deviget Test Website"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_versioning" "versioning_deviget-s3-bucket" {
  bucket = aws_s3_bucket.deviget-s3-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_dynamodb_table" "terraform-locks" {
  name         = "terraform-locks-deviget-2022"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

terraform {
  backend "s3" {
    bucket = "deviget-test-bucket"
    key = "terraform.tfstate"
    region = "us-west-2"
    dynamodb_table = "terraform-locks-deviget-2022"
    encrypt = true
  }
}

resource "aws_s3_bucket_public_access_block" "deviget-s3-bucket" {
  bucket = aws_s3_bucket.deviget-s3-bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_kms_key" "deviget-bucket-key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}

#resource "aws_s3_bucket_server_side_encryption_configuration" "deviget-s3-bucket" {
#  bucket = aws_s3_bucket.deviget-s3-bucket.bucket
#
#  rule {
#    apply_server_side_encryption_by_default {
#      kms_master_key_id = aws_kms_key.deviget-bucket-key.arn
#      sse_algorithm     = "aws:kms"
#    }
#  }
#}

// Use the AWS Certificate Manager to create an SSL cert for our domain.
// This resource won't be created until you receive the email verifying you
// own the domain and you click on the confirmation link.
resource "aws_acm_certificate" "certificate" {
  // We want a wildcard cert so we can host subdomains later.
  domain_name       = "*.${var.root_domain_name}"
  validation_method = "EMAIL"

  // We also want the cert to be valid for the root domain even though we'll be
  // redirecting to the www. domain immediately.
  subject_alternative_names = ["${var.root_domain_name}"]
}

resource "aws_cloudfront_distribution" "www_distribution" {
  // origin is where CloudFront gets its content from.
  origin {
    // We need to set up a "custom" origin because otherwise CloudFront won't
    // redirect traffic from the root domain to the www domain, that is from
    // runatlantis.io to www.runatlantis.io.
    custom_origin_config {
      // These are all the defaults.
      http_port              = "80"
      https_port             = "443"
      origin_protocol_policy = "http-only"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }

    // Here we're using our S3 bucket's URL!
    domain_name = "${aws_s3_bucket.deviget-s3-bucket.website_endpoint}"
    // This can be any name to identify this origin.
    origin_id   = "${var.www_domain_name}"
  }

  enabled             = true
  default_root_object = "index.html"

  // All values are defaults from the AWS console.
  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    // This needs to match the `origin_id` above.
    target_origin_id       = "${var.www_domain_name}"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  // Here we're ensuring we can hit this distribution using www.runatlantis.io
  // rather than the domain name CloudFront gives us.
  aliases = ["${var.www_domain_name}"]

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  // Here's where our certificate is loaded in!
  viewer_certificate {
    acm_certificate_arn = "${aws_acm_certificate.certificate.arn}"
    ssl_support_method  = "sni-only"
  }
}

resource "aws_s3_object" "object" {
  bucket = "deviget-test-bucket"
  key    = "index.html"
  source = "./site/index.html"
  content_type = "text/html"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("./site/index.html")
}

resource "aws_s3_bucket_acl" "deviget-s3-bucket-acl" {
  bucket = aws_s3_bucket.deviget-s3-bucket.id
  acl    = "public-read"
}

# create bucket policy
resource "aws_s3_bucket_policy" "deviget-s3-bucket-policy" {
  bucket = aws_s3_bucket.deviget-s3-bucket.id

  policy = <<POLICY
  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Principal": "*",
              "Action": "s3:GetObject",
              "Resource": "${aws_s3_bucket.deviget-s3-bucket.arn}/*"
          }
      ]
  }
  POLICY
}

variable "mime_types" {
  default = {
    htm   = "text/html"
    html  = "text/html"
    css   = "text/css"
    js    = "application/javascript"
    map   = "application/javascript"
    json  = "application/json"
    png   = "image/png"
    jpg   = "image/jpg"
    jpeg  = "image/jpg"
    gif   = "image/gif"
    svg   = "image/svg+xml"
  }
}

resource "aws_s3_bucket_website_configuration" "deviget-s3-bucket" {
  bucket = aws_s3_bucket.deviget-s3-bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }
}